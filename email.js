(function() {
    function decode(text, shift) {
        var shift = shift || -2,
            out = "";
        for (var i = 0, l = text.length; i < l; i++) {
            out += String.fromCharCode(text.charCodeAt(i) + shift);
        }
        return out;
    }

    function fix(el) {
        el.setAttribute("href", decode(el.getAttribute("data-href")));
        el.innerHTML = decode(el.innerHTML);
    }

    var fixed = false;
    function fixEmail() {
        if (fixed) return;
        fixed = true;
        fix(document.getElementById("email"));
    }

    function addEvent(target, name, listener) {
       if (target.addEventListener) {
           target.addEventListener(name, listener, false);
       } else if (target.attachEvent) {
           target.attachEvent('on' + name, listener);
       }
    }

    addEvent(window, 'load', fixEmail);
    addEvent(window, 'DOMContentLoaded', fixEmail);
})();
